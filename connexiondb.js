
const mongoose = require("mongoose");
require("dotenv").config();

// Required fields for a successful connection
const connectionParams = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
};
//const uri = `mongodb+srv://<username>:<password>@cluster0.lu9td.mongodb.net/?retryWrites=true&w=majority`;
const uri = `mongodb+srv://oussama:oussama@cluster0.lu9td.mongodb.net/CRUD_APP?retryWrites=true&w=majority`

// Connect to MongoDB
const connexion = mongoose
  .connect(uri, connectionParams)
  .then(() => {
    console.log("Connected to MongoDB");
  })
  .catch((error) => {
    console.error("Error connecting to MongoDB:", error.message);
  });

module.exports = connexion;
