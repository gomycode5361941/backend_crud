const mongoose = require("mongoose");

const schema = mongoose.Schema(
  {
    Name: "String",
    Adress: "String",
    Age: "Number",
    imageUrl: "String", // Add this field for image URL

  },
  {
    timestamps: true,
  }
);

const Post = mongoose.model("Post", schema);
module.exports = Post;
