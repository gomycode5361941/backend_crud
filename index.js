const express = require('express');
const mongoose = require('mongoose');
const app = express();
require('./connection')
var cors = require('cors')

app.use(cors())

const postModel = require('./postModel')

app.use(express.urlencoded({extended:true}))
app.use(express.json());

//crud applications
app.post('/', async(req, res) =>{
    const {Name, Adress, Age} = req.body
    try{
        const newPost = await postModel.create({Name, Adress, Age});
        res.json(newPost)
    }catch(error){
        res.status(500).send(error)
    }
})
//getting all the data from db
app.get('/', async(req, res) =>{
    try {
        const posts = await postModel.find()
        res.json(posts)
    } catch (error) {
        res.status(500).send(error)
    }
})

app.listen(3001, ()=>{
    console.log('The app is listening to the port: http://localhost:3001');
}) 
